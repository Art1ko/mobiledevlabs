﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MobileDev_lab1
{
    public partial class MainPage : ContentPage
    {

        private static Dictionary<string, Color> nameToColor = new Dictionary<string, Color>
        {
            { "Красный", Color.Red }, { "Черный", Color.Black },
            { "Синий", Color.Blue },  { "Желтый", Color.Yellow },
            { "Серый", Color.Gray }, { "Зеленый", Color.Green }

        };
        public MainPage()
        {
            InitializeComponent();
            ColorCanvas.Children.Add(CreateColorPicker());
            
        }
        private Picker CreateColorPicker()
        {
            Picker colorPicker = new Picker();
            colorPicker.Title = "Выберите цвет";
            foreach (string colorName in nameToColor.Keys)
            {
                colorPicker.Items.Add(colorName);
            }

            colorPicker.SelectedIndexChanged += (sender, e) =>
            {
                if (colorPicker.SelectedIndex == -1)
                {
                    ColoredTextLabel.TextColor = Color.Default;
                }
                else
                {
                    string colorName = colorPicker.Items[colorPicker.SelectedIndex];
                    ColoredTextLabel.TextColor = nameToColor[colorName];
                }

            };
            return colorPicker;
        }

        private  void OkButton_Clicked(object sender, EventArgs e)
        {
            OutputLabel.Text = ColoredTextLabel.Text;
            OutputLabel.TextColor = ColoredTextLabel.TextColor;
        }

        private void CancelButton_Clicked(object sender, EventArgs e)
        {
            OutputLabel.Text = "";
        }
    }
}
